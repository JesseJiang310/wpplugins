=== Force Login ===
Contributors: Jesse
Tags: buy on website
Stable tag: 1.0
License: GPLv2 or later

Extra Fee is a plugin that can be used to add "buy on other website" button to product detail page

== Description ==

add "buy on other website" button to product detail page.
for example, you can use this plugin to link to amazon product page.

**Features**



**Bug Reports**



== Installation ==

Upload the Force Login plugin to your site, then Activate it.