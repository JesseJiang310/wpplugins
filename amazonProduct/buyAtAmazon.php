<?php
/**
 * Copyright 2019  Jesse  (email : JesseJiang310@gmail.com)
 * @package Buy On Amazon
 * @version 1.0
 */
/*
Plugin Name: Buy On Other Website
Plugin URI:
Description: This is a  plugin that can be used to add "buy on other website" button to product detail page
Author: Jesse Jiang
Version: 1.0
Author URI: http://www.jiang.co.nz
*/


function prefix_add_text_input() {
	$label = 'Buy On Amazon';
	$option = get_option('buy_on_website_option_name');
	if( isset( $option['website_label'] ) )
		$label = sanitize_text_field( $option['website_label'] );
	$args = array(
		'label' => $label.' Product Url', // Text in the label in the editor.
		'placeholder' => 'https://', // Give examples or suggestions as placeholder
		'class' => '',
		'style' => '',
		'wrapper_class' => '',
		'id' => '_external_product', // required, will be used as meta_key
		'name' => '_external_product', // name will be set automatically from id if empty
		'type' => 'text',
		'desc_tip' => $label.' product url',
		'data_type' => '',
		'custom_attributes' => '',
		'description' => $label.' product url, if set url here, it will show buy at amazon button on the product page'
	);
	woocommerce_wp_text_input( $args );
}
add_action( 'woocommerce_product_options_tax', 'prefix_add_text_input' );
function save_amazon_meta( $post_id ) {
	// grab the SKU value
	$amaonUrl = isset( $_POST[ '_external_product' ] ) ? sanitize_text_field( $_POST[ '_external_product' ] ) : '';

	// grab the product
	$product = wc_get_product( $post_id );

	// save the custom SKU meta field
	$product->update_meta_data( '_external_product', $amaonUrl );
	$product->save();
}
add_action( 'woocommerce_process_product_meta', 'save_amazon_meta' );

add_action( 'woocommerce_after_add_to_cart_form', 'add_buy_at_amazon_button', 5 );

function add_buy_at_amazon_button() {
	$label = 'Buy On Amazon';
	$option = get_option('buy_on_website_option_name');
	if( isset( $option['website_label'] ) )
		$label = 'Buy On '.sanitize_text_field( $option['website_label'] );
		global $product;
		$meta = get_post_meta( $product->get_id(), '_external_product',true);
		if(!empty( $meta ) ){
					echo '<a style="margin-left:107px;" class="single_add_to_cart_button button alt" href="'.$meta.'" target="_blank">'.$label.'</a>';
		}
}



class BuyOnWebsiteSettingPage
{
	/**
	 * Holds the values to be used in the fields callbacks
	 */
	private $options;

	/**
	 * Start up
	 */
	public function __construct()
	{
		add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
		add_action( 'admin_init', array( $this, 'page_init' ) );
	}

	/**
	 * Add options page
	 */
	public function add_plugin_page()
	{
		// This page will be under "Settings", $page_title, $menu_title, $capability, $menu_slug, $function
		add_options_page(
			'Buy On Website Settings',
			'Buy On Website Settings',
			'manage_options',
			'buy-website-setting-admin',
			array( $this, 'create_buy_on_website_setting_page' )
		);
	}

	/**
	 * Options page callback
	 */
	public function create_buy_on_website_setting_page()
	{
		// Set class property
		$this->options = get_option( 'buy_on_website_option_name' );
		?>
		<div class="wrap">
			<h1>Buy On Website Settings</h1>
			<form method="post" action="options.php">
				<?php
				// This prints out all hidden setting fields
				settings_fields( 'buy_on_website_option_group' );
				do_settings_sections( 'buy-website-setting-admin' );
				submit_button();
				?>
			</form>
		</div>
		<?php
	}

	/**
	 * Register and add settings
	 */
	public function page_init()
	{
		register_setting(
			'buy_on_website_option_group', // Option group
			'buy_on_website_option_name', // Option name
			array( $this, 'sanitize' ) // Sanitize
		);

		add_settings_section(
			'buy_on_website_setting_section_id', // ID
			'Buy On Website Settings', // Title
			array( $this, 'print_section_info' ), // Callback
			'buy-website-setting-admin' // Page
		);

		add_settings_field(
			'website_label', // ID
			'external website name', // Title
			array( $this, 'website_label_callback' ), // Callback
			'buy-website-setting-admin', // Page
			'buy_on_website_setting_section_id' // Section
		);

	}

	/**
	 * Sanitize each setting field as needed
	 *
	 * @param array $input Contains all settings fields as array keys
	 */
	public function sanitize( $input )
	{
		$new_input = array();
		if( isset( $input['website_label'] ) )
			$new_input['website_label'] = sanitize_text_field( $input['website_label'] );

		return $new_input;
	}

	/**
	 * Print the Section text
	 */
	public function print_section_info()
	{
		print 'Enter your settings below:';
	}

	/**
	 * Get the settings option array and print one of its values
	 */
	public function website_label_callback()
	{
		printf(
			'<input type="text" id="extraFeeLabel" name="buy_on_website_option_name[website_label]" value="%s" />',
			isset( $this->options['website_label'] ) ? esc_attr( $this->options['website_label']) : ''
		);
	}

}

if( is_admin() )
	$my_settings_page = new BuyOnWebsiteSettingPage();
?>
